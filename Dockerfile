FROM ubuntu:bionic

RUN apt update -y \
  && apt-get install -y build-essential autoconf libtool gawk alien fakeroot wget zlib1g-dev uuid-dev libattr1-dev libblkid-dev libselinux-dev libudev-dev libaio-dev parted lsscsi ksh libssl-dev libelf-dev python3 python3-dev python3-setuptools python3-cffi make curl gnupg2 moreutils

WORKDIR /build

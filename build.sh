#!/usr/bin/env bash

set -xeu

echo "### Installing prerequisites..."
apt update -y
apt install -y linux-headers-${KERNEL_VERSION}


echo "### Downloading ZFS source..."
wget -c "https://github.com/zfsonlinux/zfs/releases/download/zfs-${ZFS_VERSION}/zfs-${ZFS_VERSION}.tar.gz"
wget -c "https://github.com/zfsonlinux/zfs/releases/download/zfs-${ZFS_VERSION}/zfs-${ZFS_VERSION}.tar.gz.asc"

echo "### Importing ZFS maintainer GPG keys"
gpg --import <(curl "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xB97467AAC77B9667") || true
gpg --import <(curl "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x6AD860EED4598027") || true
gpg --import <(curl "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x0AB9E991C6AF658B") || true

echo "### Verifying source signature..."
gpg --verify "zfs-${ZFS_VERSION}.tar.gz.asc"

echo "### Extracting source..."
tar xzf "zfs-${ZFS_VERSION}.tar.gz"

cd "zfs-${ZFS_VERSION}"

echo "### Compiling ZFS..."
COMPILTE_TIME="$(date +%s)"
sh autogen.sh
./configure \
  --prefix=/usr \
  --with-config=user
make -s -j$(($(nproc) + 1))
make -j1 deb

cd ..

echo "### Patching packages..."

fixpkg() {
  local tmpDir="$(mktemp -d)"
  mkdir -p "${tmpDir}"
  dpkg -I "${1}"
  dpkg-deb -c "${1}"
  dpkg-deb -R "${1}" "$tmpDir"
  rm "${1}"
  if [[ $1 == *"kmod-"* ]]; then
    echo -e "Depends: linux-headers-${KERNEL_VERSION}, linux-image-${KERNEL_VERSION}\n$(cat $tmpDir/DEBIAN/control)" > "${tmpDir}/DEBIAN/control"
  fi
  awk '$1~/Version:/ {print $1 " 1:" $2 "+gitlab'"${COMPILTE_TIME}"'"}; $1!~/Version:/ {print $0}' "$tmpDir/DEBIAN/control" | sponge "$tmpDir/DEBIAN/control"
  dpkg-deb -b "${tmpDir}" "${1}"
  rm -rf "${tmpDir}"
}

for deb in $(find . -type f -name '*.deb'); do
  fixpkg "${deb}"
done

echo "### Done"
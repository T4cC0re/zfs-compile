.EXPORT_ALL_VARIABLES:
KERNEL_VERSION ?= 4.15.0-1027-gcp
ZFS_VERSION ?= 0.8.0
IMAGE_NAME ?= registry.gitlab.com/t4cc0re/zfs-compile

DOCKER_VARS := KERNEL_VERSION ZFS_VERSION

.PHONY: all
all: docker build

.PHONY: docker
docker: Dockerfile
	@docker build -t $(IMAGE_NAME) .

.PHONY: build
build: build.sh
	docker run --rm -it -v "$(shell pwd)/:/build" $(foreach var,$(DOCKER_VARS), -e "$(var)=$($(var))") $(IMAGE_NAME) /build/build.sh

.PHONY: incontainer
incontainer: build.sh
	./build.sh

.PHONY: clean
clean:
	rm -rf linux-* zfs-* spl-*
